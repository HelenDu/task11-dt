import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Data {
	private ArrayList<String[]> datas;
	private HashMap<Integer,String[]> attributes;
	private ArrayList<String[]> classifyDatas;
	String source;
	public Data(String s) {
		this.datas = new ArrayList<String[]>();
		this.attributes = new HashMap<Integer, String[]>();
		this.classifyDatas = new ArrayList<String[]>();
		this.source = s;
	}
	
	@SuppressWarnings("null")
	void readdata() {
		try {
			File f = new File(source);
			BufferedReader br;
			br = new BufferedReader(new FileReader(f));
			String str = null;
			this.datas = new ArrayList<String[]>();
			int x = 0;
			while((str=br.readLine()) != null && !str.matches("@data")){
				if(!str.isEmpty()){
					if(str.startsWith("@attribute")){
						String[] attr = str.split(" ");
						if(attr[2].equals("real")){
							String[] real = new String[1];
							real[0]=attr[2];
							this.attributes.put(x, real);
							x++;
							continue;
						}
						String[] attrs = attr[2].substring(1, attr[2].length()-1).split(",");
						this.attributes.put(x, attrs);
						x++;
					}
				}
			}
			//for(int i=0;i<this.attributes.size();i++){
			//	System.out.println(i+"="+Arrays.toString(this.attributes.get(i)));
			//}
			while (br != null) {
				str = br.readLine();
				String[] temp = datahandle(str);
				this.datas.add(temp);
			}
			br.close();
		} catch (Exception e) {
		}
	}
	void readClassifyData(String testSource) {
		try {
			File f = new File(testSource);
			BufferedReader br;
			br = new BufferedReader(new FileReader(f));
			String str = null;
			this.classifyDatas = new ArrayList<String[]>();
			while((str=br.readLine()) != null && !str.matches("@data")){
				continue;
			}
			while (br != null) {
				str = br.readLine();
				String[] temp = datahandle(str);
				this.classifyDatas.add(temp);
			}
			br.close();
		} catch (Exception e) {
		}
	}
	
	public String[] datahandle(String str) {
		String[] d = str.split(",");
		return d;
	}

	public void displayData(ArrayList<String[]> datas) {
		for (int i = 0; i < datas.size(); i++) {
			System.out.println(Arrays.toString(datas.get(i)));
			for (int j = 0; j < datas.get(i).length; j++) {
				System.out.print(datas.get(i)[j] + " ");
			}
			System.out.println();
		}
	}
	public ArrayList<String[]> getDatas() {
		return datas;
	}

	public HashMap<Integer, String[]> getAttributes() {
		return attributes;
	}

	public ArrayList<String[]> getClassifyDatas() {
		return classifyDatas;
	}

	public void setDatas(ArrayList<String[]> datas) {
		this.datas = datas;
	}

	public void setAttributes(HashMap<Integer, String[]> attributes) {
		this.attributes = attributes;
	}

	public void setClassifyDatas(ArrayList<String[]> classifyDatas) {
		this.classifyDatas = classifyDatas;
	}
	/**public static void main(String[] args) {
		Data dataPre = new Data();
		dataPre.readdata();
		Randoms rData = new Randoms(dataPre);
		rData.random();
		ArrayList<String[]> trainingData = rData.getData();
		ArrayList<String[]> testData = new ArrayList<String[]>();
		while(testData.size()<30){
			testData.add(trainingData.remove(0));
		}
		
		DecisionTree dt = new DecisionTree(testData,dataPre.attributes);
		TreeNode root = new TreeNode(trainingData, null, dataPre.attributes);
		dt.creatTree(root,root.dataset.size()+1);
		
		dt.displayTree(root);
		dt.testTree(root);
	}*/
}
