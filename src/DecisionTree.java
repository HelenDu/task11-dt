import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class DecisionTree {
	private int attributesNum;// not include label
	private HashMap<Integer, String[]> attributesType; //Map of Attribute and its Values
	private static double[] threshold = { 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3 }; // PART A
	//private static double[] threshold = { 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1 }; //PART B
	private String[] results;
	private ArrayList<String[]> testData = null;
	LinkedList<TreeNode> s = new LinkedList<TreeNode>();
	//private double confidenceFactor = 0.25;
	private ArrayList<String[]> classifyData = null;

	public DecisionTree(ArrayList<String[]> testData, HashMap<Integer, String[]> attributesType,
			ArrayList<String[]> classifyData) {
		this.testData = testData;
		this.attributesType = attributesType;
		this.attributesNum = this.attributesType.size() - 1;
		this.classifyData = classifyData;
	}

	public void createTree(TreeNode root, int preSize) {
		// Pre-pruning
		// size
		if (root == null || root.dataset.size() < 5) {// PART A
		//if (root == null || root.dataset.size() < 3) {//PART B
			return;
		}
		// purity
		int labelNum = attributesType.get(attributesNum).length;
		int[] labelSum = new int[labelNum];
		for (int i = 0; i < labelNum; i++) {
			labelSum[i] = 0;
		}
		for (int i = 0; i < root.dataset.size(); i++) {
			for (int j = 0; j < attributesType.get(attributesNum).length; j++) {
				if (root.dataset.get(i).equals(
						attributesType.get(attributesNum)[j])) {
					labelSum[j]++;
					continue;
				}
			}
		}
		for (int i = 0; i < labelNum; i++) {
			double purity = labelSum[i] / root.dataset.size();
			if (purity > 0.93) {
				return;
			}
		}
		if (preSize == root.dataset.size()) {
			return;
		}
		// IGR < threshold STOP
		Attribute tmpAttribute = root.getAttribute();
		if (tmpAttribute == null || tmpAttribute.number < 0) {
			// System.out.println("No Attributes");
			return;
		}
		if (tmpAttribute.IGR < threshold[tmpAttribute.number]) {
			// //System.out.println("IGR <= "+ threshold[tmp.number]);
			return;
		}
		// build Tree
		root.getChildSets(tmpAttribute);
		// leaf Node
		if (root.childSets == null) {
			return;
		}
		for (int i = 0; i < root.childSets.size(); i++) {
			createTree(root.childSets.get(i), root.dataset.size());
		}
	}
	
	//Classify the unlabeled data from testXXX.arff.
	public void classify(TreeNode root) {
		System.out.println("START CLASSIFY:");
		this.results = new String[classifyData.size()];
		for (int i = 0; i < classifyData.size(); i++) {
			TreeNode nowNode = root;
			if (root == null) {
				return;
			}
			while (nowNode.childSets != null && nowNode.childSets.size() > 0) {
				// Discrete
				int aN = nowNode.attribute.number;
				int Num = attributesType.get(aN).length;
				if (Num == 1) {
					try {
						double d = Double
								.parseDouble(this.classifyData.get(i)[aN]);
						if (d < nowNode.attribute.threshold) {
							nowNode = nowNode.childSets.get(0);
						} else {
							nowNode = nowNode.childSets.get(1);
						}
					} catch (Exception e) {
						System.out.println("Parse failed");
					}
				} else {
					// Contigous
					for (int j = 0; j < Num; j++) {
						if (this.classifyData.get(i)[aN].equals(attributesType
								.get(aN)[j])) {
							nowNode = nowNode.childSets.get(j);
							continue;
						}
					}
				}
			}
			int CNum = attributesType.get(attributesNum).length;
			int[] vote = new int[CNum];
			for (int j = 0; j < CNum; j++) {
				vote[j] = 0;
			}
			for (int j = 0; j < nowNode.dataset.size(); j++) {
				for (int n = 0; n < CNum; n++) {
					if (nowNode.dataset.get(j)[attributesNum]
							.equals(attributesType.get(attributesNum)[n])) {
						vote[n]++;
						continue;
					}
				}
			}
			int rNum = 0;
			int maxVote = vote[0];
			for (int j = 1; j < CNum; j++) {
				if (maxVote < vote[j]) {
					maxVote = vote[j];
					rNum = j;
				}
			}
			results[i] = attributesType.get(attributesNum)[rNum];
		}
		for (int i = 0; i < classifyData.size(); i++) {
			System.out.println(this.results[i]);
		}
		System.out.println("END CLASSIFY~");
	}

	//get the accuracy of the DT by classify the labeled data, then get the precision
	public double testTree(TreeNode root) {
		int precision = 0;
		results = new String[this.testData.size()];
		for (int i = 0; i < this.testData.size(); i++) {
			TreeNode nowNode = root;
			results[i] = "";
			if (root == null) {
				return -1;
			}
			while (nowNode.childSets != null && nowNode.childSets.size() > 0) {
				int aN = nowNode.attribute.number;
				int Num = attributesType.get(aN).length;
				if (Num == 1) {
					// Continous
					try {
						double d = Double.parseDouble(this.testData.get(i)[aN]);
						if (d < nowNode.attribute.threshold) {
							nowNode = nowNode.childSets.get(0);
						} else {
							nowNode = nowNode.childSets.get(1);
						}
					} catch (Exception e) {
						System.out.println("Parse failed");
					}
				} else {
					// Discrete
					for (int j = 0; j < Num; j++) {
						if (this.testData.get(i)[aN].equals(attributesType
								.get(aN)[j])) {
							nowNode = nowNode.childSets.get(j);
							break;
						}
					}
				}
			}
			int CNum = attributesType.get(attributesNum).length;

			int[] vote = new int[CNum];
			for (int j = 0; j < CNum; j++) {
				vote[j] = 0;
			}
			for (int j = 0; j < nowNode.dataset.size(); j++) {
				for (int n = 0; n < CNum; n++) {
					if (nowNode.dataset.get(j)[attributesNum]
							.equals(attributesType.get(attributesNum)[n])) {
						vote[n]++;
						continue;
					}
				}
			}
			int rNum = 0;
			int maxVote = vote[0];
			for (int j = 1; j < CNum; j++) {
				if (maxVote < vote[j]) {
					maxVote = vote[j];
					rNum = j;
				}
			}
			String target = "";
			target = this.testData.get(i)[attributesNum];
			results[i] = attributesType.get(attributesNum)[rNum];
			if (results[i].equals(target)) {
				precision++;
			} else {
				// System.out.println(results[i] + " : " + target + " : "
				// + attributesNum);
			}
		}
		double accuracy = 1.0 * precision / this.testData.size();
		// System.out.println("accuracy="+accuracy);
		return accuracy;
	}
	
	
/** not used 
	//get the training data's precision
	public double getTrainingPrecision(TreeNode root) {
		int precision = 0;
		ArrayList<String[]> precisionData = root.dataset;
		results = new String[precisionData.size()];
		for (int i = 0; i < precisionData.size(); i++) {
			TreeNode nowNode = root;
			results[i] = "";
			if (root == null) {
				return -1;
			}
			while (nowNode.childSets != null && nowNode.childSets.size() > 0) {
				// Discrete
				int aN = nowNode.attribute.number;
				int Num = attributesType.get(aN).length;
				if (Num == 1) {
					try {
						double d = Double.parseDouble(precisionData.get(i)[aN]);
						if (d < nowNode.attribute.threshold) {
							nowNode = nowNode.childSets.get(0);
						} else {
							nowNode = nowNode.childSets.get(1);
						}
					} catch (Exception e) {
						System.out.println("Parse failed");
					}
				} else {
					// Contigous
					for (int j = 0; j < Num; j++) {
						if (precisionData.get(i)[aN].equals(attributesType
								.get(aN)[j])) {
							nowNode = nowNode.childSets.get(j);
							continue;
						}
					}
				}
			}
			int CNum = attributesType.get(attributesNum).length;
			int[] vote = new int[CNum];
			for (int j = 0; j < CNum; j++) {
				vote[j] = 0;
			}
			for (int j = 0; j < nowNode.dataset.size(); j++) {
				for (int n = 0; n < CNum; n++) {
					if (precisionData.get(i)[attributesNum]
							.equals(attributesType.get(attributesNum)[n])) {
						vote[n]++;
						continue;
					}
				}
			}
			int rNum = 0;
			int maxVote = vote[0];
			for (int j = 1; j < CNum; j++) {
				if (maxVote < vote[j]) {
					maxVote = vote[j];
					rNum = j;
				}
			}
			String target = "";
			target = precisionData.get(i)[attributesNum];
			results[i] = attributesType.get(attributesNum)[rNum];
			if (results[i].equals(target)) {
				precision++;
			} else {
				// //System.out.println(results[i] + " : " + target + " : "+
				// attributesNum);
			}
		}
		double accuracy = 1.0 * precision / precisionData.size();
		// //System.out.println("precision accruacy="+accuracy);
		return accuracy;
	}

	public double getConfidenceInterval(double confidenceFactor, double p, int N) {
		return (p + confidenceFactor * Math.sqrt(p * (1 - p) / N));
	}

	public TreeNode postPruning(TreeNode root) {
		int precisionSize = root.dataset.size();
		double p1 = this.getTrainingPrecision(root);
		double c1 = getConfidenceInterval(confidenceFactor, p1, precisionSize);
		TreeNode root2 = root;
		root2 = pruning(root2, c1);
		double p2 = this.getTrainingPrecision(root2);
		double c2 = getConfidenceInterval(confidenceFactor, p2, precisionSize);
		if (c2 < c1) {
			return root2;
		} else {
			return root;
		}
	}

	public TreeNode pruning(TreeNode root, double c1) {
		// TODO 遍历树， 凡是叶子节点，则找parent，算值，判断是否剪枝
		return null;
	}*/

}
