import java.util.ArrayList;

public class CorssValidate {

	public static void main(String[] args) {
		String fileInput;
		String testInput;
		//TODO set up the files path
		fileInput = "trainProdSelection.arff";//PART A
		testInput = "testProdSelection.arff"; //PART A
		// fileInput = "trainProdIntro.binary.arff";//PART B
		// testInuput = "testProdIntro.binary.arff";//PART B
		
		Data dataPre = new Data(fileInput);
		dataPre.readdata();
		dataPre.readClassifyData(testInput);
		ArrayList<String[]> initalDataSet = dataPre.getDatas();
		
		// To better observation the random data, let it do 10 times of k Fold Cross Validate
		for(int num =0;num<10;num++) {
			Randoms rData = new Randoms(dataPre);
			rData.random();
			initalDataSet = rData.getData();

			// TODO set up K
			int k = 5;
			int size_test = initalDataSet.size() / k;
			int mod = initalDataSet.size() % k;
			double[] accuracy = new double[k + 1];

			for (int i = 0; i < mod; i++) {
				ArrayList<String[]> testDataSet = new ArrayList<String[]>();
				ArrayList<String[]> trainingDataSet = new ArrayList<String[]>();
				for (int n = 0; n < initalDataSet.size(); n++) {
					trainingDataSet.add(initalDataSet.get(n));
				}
				for (int j = 0; j < size_test + 1; j++) {
					testDataSet.add(trainingDataSet.get(0));
					trainingDataSet.remove(0);
					initalDataSet.add(initalDataSet.remove(0));
				}
				// run trainingDataSet testDataSet
				DecisionTree dt = new DecisionTree(testDataSet,
						dataPre.getAttributes(), dataPre.getClassifyDatas());
				TreeNode root = new TreeNode(trainingDataSet,
						new ArrayList<Attribute>(), dataPre.getAttributes(),
						null, 0);
				dt.createTree(root, root.dataset.size() + 1);
				// root.printTree();
				// GET ACCURICE
				double tmpAccuracy = dt.testTree(root);
				accuracy[i] = tmpAccuracy;
			}
			for (int i = mod; i < k; i++) {
				ArrayList<String[]> testDataSet = new ArrayList<String[]>();
				ArrayList<String[]> trainingDataSet = new ArrayList<String[]>();
				for (int n = 0; n < initalDataSet.size(); n++) {
					trainingDataSet.add(initalDataSet.get(n));
				}
				for (int j = 0; j < size_test; j++) {
					testDataSet.add(trainingDataSet.get(0));
					trainingDataSet.remove(0);
					initalDataSet.add(initalDataSet.remove(0));
				}
				// run trainingDataSet testDataSet
				DecisionTree dt = new DecisionTree(testDataSet,
						dataPre.getAttributes(), dataPre.getClassifyDatas());
				TreeNode root = new TreeNode(trainingDataSet,
						new ArrayList<Attribute>(), dataPre.getAttributes(),
						null, 0);
				dt.createTree(root, root.dataset.size() + 1);
				// root.printTree();
				// GET ACCURICE
				double tmpAccuracy = dt.testTree(root);
				accuracy[i] = tmpAccuracy;
			}
			accuracy[k] = 0.0;
			for (int j = 0; j < k; j++) {
				accuracy[k] = accuracy[k] + accuracy[j];
				// System.out.println("acuracy" + j + ":" + accuracy[j]);
			}
			accuracy[k] = 1.0 * accuracy[k] / k;
			System.out.println("Cross Validate Accuracy : " + accuracy[k]);
		}

		/***************Classify*********************************/
		// Use the whole training data to generate the Decision Tree, print it, and print the results of unlabeled data
		ArrayList<String[]> trainingDataSet = new ArrayList<String[]>();
		for (int n = 0; n < initalDataSet.size(); n++) {
			trainingDataSet.add(initalDataSet.get(n));
		}
		DecisionTree dt = new DecisionTree(null, dataPre.getAttributes(),
				dataPre.getClassifyDatas());
		TreeNode root = new TreeNode(initalDataSet, new ArrayList<Attribute>(),
				dataPre.getAttributes(), null, 0);
		dt.createTree(root, root.dataset.size() + 1);
		root.printTree();
		dt.classify(root);

	}
}