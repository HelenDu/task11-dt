
public class Attribute {
	int number;
	double threshold;
	double IGR;
	
	public Attribute(int num, double threshold, double igr) {
		this.number = num;
		this.threshold = threshold;
		this.IGR = igr;
	}

	public int getNumber() {
		return number;
	}

	public double getThreshold() {
		return threshold;
	}

	public double getIGR() {
		return IGR;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public void setThreshold(double threshold) {
		this.threshold = threshold;
	}

	public void setIGR(double iGR) {
		IGR = iGR;
	}

}
