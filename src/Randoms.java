import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class Randoms {

	private ArrayList<String[]> data;
	private Data initdata;

	public Randoms(Data d) {
		data = new ArrayList<String[]>();
		initdata = d;
	}

	public void random() // k folds
	{
		for (int i = 0; i < this.initdata.getDatas().size(); i++) {
			this.data.add(this.initdata.getDatas().get(i));
		}
		ArrayList<String[]> tmp = new ArrayList<String[]>();

		while (this.data.size() > 0) {
			double rd = Math.random();
			int x = (int) (rd * this.data.size());
			tmp.add(this.data.get(x));
			this.data.remove(x);
		}
		for (int i = 0; i < tmp.size(); i++) {
			this.data.add(tmp.get(i));
		}

		File f = new File("10Folds.txt");
		BufferedWriter bw;
		try {
			bw = new BufferedWriter(new FileWriter(f));
			for (int i = 0; i < this.data.size(); i++) {
				for (int j = 0; j < this.data.get(i).length; j++) {
					bw.write(this.data.get(i)[j] + " ");
				}
				bw.write("\n");
			}
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public ArrayList<String[]> getData() {
		return data;
	}

	public Data getInitdata() {
		return initdata;
	}

	public void setData(ArrayList<String[]> data) {
		this.data = data;
	}

	public void setInitdata(Data initdata) {
		this.initdata = initdata;
	}
}
