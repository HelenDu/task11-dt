import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class TreeNode {
	public int depth = 0;
	ArrayList<TreeNode> childSets;
	ArrayList<String[]> dataset = null;
	HashMap<Integer, String[]> attributesType;
	int attributesNum;// not include label
	ArrayList<Attribute> parentAttributs = new ArrayList<Attribute>();
	Attribute attribute = null;
	
	public TreeNode(ArrayList<String[]> dataset,
			ArrayList<Attribute> fatherAttributs,
			HashMap<Integer, String[]> attributsType, TreeNode parent, int depth) {
		this.dataset = dataset;
		this.depth = depth;
		this.parentAttributs = fatherAttributs;
		this.childSets = new ArrayList<TreeNode>();
		this.attribute = new Attribute(-1, -1, Double.MIN_VALUE);
		this.attributesType = attributsType;
		this.attributesNum = this.attributesType.size() - 1;
	}

	public TreeNode() {
		this.dataset = new ArrayList<String[]>();
	}

	//Split data to childSets by the Attribute's threshold or values
	public void getChildSets(Attribute attribute) {
		if (attribute == null || attribute.number < 0) {
			this.childSets = null;
			this.attribute = null;
			return;
		}
		this.childSets = new ArrayList<TreeNode>();
		this.attribute = attribute;
		ArrayList<Attribute> fAttributes;
		if (this.parentAttributs == null) {
			fAttributes = new ArrayList<Attribute>();
		} else {
			fAttributes = new ArrayList<Attribute>(this.parentAttributs);
		}
		fAttributes.add(attribute);
		// Contiguous
		if (attributesType.get(attribute.number)[0].equals("real")) {
			ArrayList<String[]> child1 = new ArrayList<String[]>();
			ArrayList<String[]> child2 = new ArrayList<String[]>();
			
			for (int i = 0; i < this.dataset.size(); i++) {
				try {
					double d = Double
							.parseDouble(this.dataset.get(i)[attribute.number]);
					if (d < attribute.getThreshold()) {
						child1.add(this.dataset.get(i));
					} else {
						child2.add(this.dataset.get(i));
					}
				} catch (Exception e) {
					System.out.println("Parse failed");
				}
			}
			this.childSets.add(new TreeNode(child1, new ArrayList<Attribute>(
					fAttributes), attributesType, this, depth + 1));
			this.childSets.add(new TreeNode(child2, new ArrayList<Attribute>(
					fAttributes), attributesType, this, depth + 1));
		} else {
			// Discrete
			ArrayList<ArrayList<String[]>> childs = new ArrayList<ArrayList<String[]>>();
			for (int j = 0; j < attributesType.get(attribute.number).length; j++) {
				childs.add(new ArrayList<String[]>());
			}

			for (int i = 0; i < this.dataset.size(); i++) {
				for (int j = 0; j < attributesType.get(attribute.number).length; j++) {
					if (this.dataset.get(i)[attribute.number]
							.equals(attributesType.get(attribute.number)[j])) {
						childs.get(j).add(this.dataset.get(i));
					}
				}
			}
			for (int j = 0; j < attributesType.get(attribute.number).length; j++) {
				this.childSets.add(new TreeNode(childs.get(j),
						new ArrayList<Attribute>(fAttributes), attributesType,
						this, depth + 1));
			}
		}
	}

	//Calculate the best Attribute by using Information Gain Ratio
	public Attribute getAttribute() {
		Attribute[] attributsIGR = new Attribute[this.attributesNum];

		for (int i = 0; i < this.attributesNum; i++) {
			if (this.attributesType.get(i).length != 1) {
				//Discrete
				if(this.attributesType.get(i).length!=1){
					//Discrete only use once
					boolean flag = true;
					for(int j=0;j<this.parentAttributs.size();j++){
						if(this.parentAttributs.get(j).number==i){
							flag = false;
						}
					}
					if(flag){
						double maxIGR = this.getDiscreteIGR(this.attributesType.get(i), i);
						attributsIGR[i] = new Attribute(i, -1, maxIGR);
					}
					else{
						attributsIGR[i] = new Attribute(i, -1, -1);
					}
				}
				
			} else {
				// Contiguous
				this.sortData(i);
				double maxThreshold = Double.MIN_VALUE;
				double maxIGR = Double.MIN_VALUE;
				for (int j = 1; j < this.dataset.size(); j++) {
					if (this.dataset.get(j)[this.attributesNum]
							.equals(this.dataset.get(j - 1)[this.attributesNum])) {
						continue;
					}
					try {
						double tmpThreshold = (Double.parseDouble(this.dataset
								.get(j)[i]) + Double.parseDouble(this.dataset
								.get(j - 1)[i])) / 2;
						double tmpIGR = this.getContiguousIGR(tmpThreshold, i);
						if (maxIGR < tmpIGR) {
							maxThreshold = tmpThreshold;
							maxIGR = tmpIGR;
						}
					} catch (Exception e) {
						// System.out.println("Parse");
					}
				}
				attributsIGR[i] = new Attribute(i, maxThreshold, maxIGR);
				//System.out.println("Contiguous:"+maxIGR);
			}
		}
		int x=0;
		int maxIndex = 0;
		double maxIGR = attributsIGR[x].IGR;
		for (int i = x + 1; i < this.attributesNum; i++) {
			if (attributsIGR[i].IGR >= maxIGR) {
				maxIndex = i;
				maxIGR = attributsIGR[i].IGR;
			}
		}
		this.attribute = attributsIGR[maxIndex];
		return this.attribute;
	}

	// sort by the x attribute
	public void sortData(final int x) {
		Collections.sort(this.dataset, new Comparator<String[]>() {
			@Override
			public int compare(String[] d1, String[] d2) {
				return Double.compare(Double.parseDouble(d1[x]),
						Double.parseDouble(d2[x]));
			}
		});
	}

	//Get the Contiguous feature's Information Gain Ratio
	public double getContiguousIGR(double threshold, int attributeNum) {
		double igr = 1.0 * this.getContiguousGain(threshold, attributeNum)
				/ this.getSplitH();
		childSets = null;
		return igr;
	}
	//Get the Discrete feature's Information Gain Ratio
	public double getDiscreteIGR(String[] attributeValue, int attributeNum) {
		double igr = 1.0 * this.getDiscreteGain(attributeValue, attributeNum)
				/ this.getSplitH();
		childSets = null;
		return igr;
	}

	//Get the SplitH
	public double getSplitH() {
		double splitH = 0.0;
		for (int i = 0; i < childSets.size(); i++) {
			double percentagei = 1.0 * childSets.get(i).dataset.size()
					/ dataset.size();
			splitH = splitH - percentagei * log2(percentagei);
		}
		// //System.out.println("SplitH="+splitH);
		return splitH;
	}

	//Get the Discrete feature's Information Gain
	public double getDiscreteGain(String[] attributeValue, int attributeNum) {
		double gain = 0.0;
		gain = getEntropy(dataset)
				- getDiscreteInfo(attributeValue, attributeNum);
		// //System.out.println("Discrete Gain="+gain);
		return gain;
	}

	//Get the Contiguous feature's Information Gain
	public double getContiguousGain(double threshold, int attributeNum) {
		double gain = 0.0;
		gain = getEntropy(dataset)
				- getContiguousInfo(threshold, attributeNum, dataset);
		// //System.out.println("Contiguous Gain="+gain);
		return gain;
	}

	//Get the Discrete feature's Information 
	public double getDiscreteInfo(String[] attributeValue, int attributeNum) {
		this.childSets = new ArrayList<TreeNode>();
		for (int i = 0; i < attributeValue.length; i++) {
			this.childSets.add(new TreeNode());
		}
		for (int x = 0; x < this.dataset.size(); x++) {
			for (int i = 0; i < attributeValue.length; i++) {
				if (dataset.get(x)[attributeNum].equals(attributeValue[i])) {
					this.childSets.get(i).dataset.add(this.dataset.get(x));
					break;
				}
			}
		}
		double information = 0.0;
		for (int i = 0; i < attributeValue.length; i++) {
			information = information + 1.0
					* this.childSets.get(i).dataset.size() / dataset.size()
					* getEntropy(this.childSets.get(i).dataset);
		}
		return information;
	}
	
	//Get the Contiguous feature's Information
	public double getContiguousInfo(double threshold, int attributeNum,
			ArrayList<String[]> dataset) {
		this.childSets = new ArrayList<TreeNode>();
		for (int i = 0; i < 2; i++) {
			this.childSets.add(new TreeNode());
		}
		for (int x = 0; x < this.dataset.size(); x++) {
			try {
				double d = Double.parseDouble(dataset.get(x)[attributeNum]);
				if (d <= threshold) {
					this.childSets.get(0).dataset.add(dataset.get(x));
				} else {
					this.childSets.get(1).dataset.add(dataset.get(x));
				}
			} catch (Exception e) {
				// System.out.println("Parse");
			}
		}
		double information = 0.0;
		for (int i = 0; i < 2; i++) {
			information = information + 1.0
					* this.childSets.get(i).dataset.size() / dataset.size()
					* getEntropy(this.childSets.get(i).dataset);
			// //System.out.println("informationi="+information);
		}
		return information;
	}

	//Get the Entropy
	public double getEntropy(ArrayList<String[]> dataset) {
		double r = 0.0;
		int[] categories = new int[this.attributesType.get(this.attributesNum).length];
		for (int i = 0; i < categories.length; i++) {
			categories[i] = 0;
		}
		for (int i = 0; i < dataset.size(); i++) {
			for (int j = 0; j < categories.length; j++) {
				if (dataset.get(i)[this.attributesNum]
						.equals(this.attributesType.get(this.attributesNum)[j])) {
					categories[j]++;
				}
			}
		}
		for (int i = 0; i < categories.length; i++) {
			double percentagei = 1.0 * categories[i] / dataset.size();
			r = r - percentagei * log2(percentagei);
		}
		// //System.out.println("Engtropy="+r);
		return r;
	}

	double log2(double value) {
		if (value == 0) {
			return 0;
		}
		double r = Math.log(value) / Math.log(2);
		return r;
	}

	public void printTree() {
		printDepthTree(0);
	}

	private void printDepthTree(int depth) {
		for (int i = 0; i < depth; i++) {
			System.out.print('-');
		}
		if (attribute == null || attribute.number == -1) {
			System.out.println("(" + depth + ",-1): [leaf]"+" :"+dataset.size());
		} else {
			if(attributesType.get(attribute.number).length==1){
				System.out.println("(" + depth + "," + attribute.number + "): "
						+ attribute.threshold+" :"+dataset.size());
			}else{
			System.out.println("(" + depth + "," + attribute.number + "): "
					+ Arrays.toString(attributesType.get(attribute.number))+" :"+dataset.size());}
		}
		if (childSets == null) {
			return;
		} else {
			for (int i = 0; i < childSets.size(); i++) {
				childSets.get(i).printDepthTree(depth + 1);
			}
		}
	}
}
