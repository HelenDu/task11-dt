Team9 Luna

Members:
    Helen Du
    Bingyang Zhao    Jiewen Mao    Jing Zhuang    Rugved SarodeProject Description:
    Implement Decision Tree by using C4.5 Algorithm.
    Use the Information Gain Ratio to select the attribute. 
    Use the Pre Pruning to prevent over fit.
    Use 10 folds cross validate to calculate the accuracy.
    In part A, our accuracy is around 87%-89%. 
    The best stop conditions are: size<5 or IGR’s thresholds<0.3.
    In part B, our accuracy is around 89%-95%. 
    The best stop conditions are: size<3 or IGR’s thresholds<0.1.

Usage: 
    Run the main function within CorssValidate.java
    Change the fileInput( & testInput ) of the file's path.
    Such as for PART A:
	String fileInput = "trainProdSelection.arff";
	String testInput = "testProdSelection.arff"; 
    Set up the threshold in DecisionTree.java
    For example: 
	private static double[] threshold = { 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3 }; 
    Then, Run it.


Reference links: 
    http://www.academia.edu/8141174/A_comparative_study_of_decision_tree_ID3_and_C4.5